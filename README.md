Чтобы развернуть проект для разработки и тестирования необходимо установить **NodeJS** и **Bower**

Для установки зависимостей и запуска сервера выполните последовательно в терминале:

1.*git clone https://sergey_gonchar@bitbucket.org/sergey_gonchar/test-task-lending.git*

2.*cd test-task-lending*

3.*bower i*

4.*npm i*

5.*gulp*