/**
 * Created by Nobodies on 12.05.2016.
 */
var gulp = require('gulp'),
    browserSync = require('browser-sync'),
    jade = require('gulp-jade'),
    sass = require('gulp-sass'),
    less = require('gulp-less'),
    path = require('path');

gulp.task('jade', function () {
    gulp.src('src/jade/*.jade')
        .pipe(jade({
            pretty: true
        }))
        .pipe(gulp.dest('./src/'));
});

//компилятор less файлов
gulp.task('less', function () {
    return gulp.src('src/less/*.less')
        .pipe(less({
            paths: [path.join(__dirname, 'less', 'includes')]
        }))
        .pipe(gulp.dest('./public/css'));
});

//компилятор sass файлов
gulp.task('sass', function () {
    gulp.src('src/sass/*.scss')
        .pipe(sass.sync().on('error', sass.logError))
        .pipe(gulp.dest('src/css'));
});

gulp.task("server", function () {
    browserSync({
        port: 9000,
        server: {
            baseDir: "src"
        }
    });

});
gulp.task('watch', function () {
    gulp.watch('src/jade/**/*', ['jade']);
    gulp.watch('src/sass/**/*', ['sass']);
    gulp.watch('src/less/**/*', ['less']);
    gulp.watch([
        'src/index.html',
        'src/*.html',
        'src/js/**/*.js',
        'src/css/**/*.css'
    ]).on('change', browserSync.reload);
});


gulp.task('default', ['server', 'watch']);